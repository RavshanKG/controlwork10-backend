create database `news-agency`;
	use `news-agency`;
    create table `news` (
		`id` int not null auto_increment primary key,
        `header` varchar (25) not null,
        `description` varchar (255),
        `pic` varchar (2550),
        `date` datetime
		);
        
	create table `comments` (
		`id` int not null auto_increment primary key,
        `news_id` int not null,
        `author` varchar (25),
        `comment` varchar (255) not null,
        foreign key (`news_id`) references `news` (`id`)
        );
const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.textname(file.originalname));
  }
});

const upload = multer({storage: storage});
const router = express.Router();

const createRouter = news_agency => {
  router.get('/news', (req, res) => {
    news_agency.query('SELECT * FROM `news`', (error, results) => {
      if (error) throw error;
      res.send(results);
    })
  })
};



/*router.get('/news/:id', (req, res) => {
  res.send('A single piece of news by id will be here');
});

router.post('/news', (req, res) => {
  res.send('Will create new piece of news here');
});

app.delete('/news:id', (req, res) => {
  res.send('Delete a single piece of news');
});*/

module.exports = router;